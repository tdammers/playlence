#pragma once

#include "WrapperModule.h"
#include <memory>

class TimeLimitModule: public WrapperModule {
	private:
		double sec_limit;
		double sec_position;
	public:
		TimeLimitModule(double _sec_limit, std::shared_ptr<Module> _child = 0);
		virtual Module* clone() const { return new TimeLimitModule(sec_limit, child_module); }
        inline TimeLimitModule& set_sec_limit(double v) { sec_limit = v; return *this; }
		virtual unsigned fill_buffer(float* buffer, const SampleParams&, unsigned num_frames);
		virtual void rewind(const SampleParams& params);
};

class RandomTimeLimitModule: public TimeLimitModule {
    private:
        double min_limit;
        double max_limit;
    public:
        RandomTimeLimitModule(double _min_limit, double _max_limit, std::shared_ptr<Module> _child = 0);
        virtual void rewind(const SampleParams& params);
};
