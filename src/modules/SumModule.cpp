#include "SumModule.h"
#include "SampleParams.h"

#include <algorithm>

unsigned SumModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {
	if (child_modules.empty()) {
		return 0;
	}
	float temp_buffer[num_frames * params.channels];
	unsigned frames_played = 0;
	unsigned frames_played_cur = 0;
	if (buffer) {
		for (unsigned p = 0; p < num_frames * params.channels; ++p) {
			buffer[p] = 0.0f;
		}
	}
	for (auto i = child_modules.begin(); i != child_modules.end(); ++i) {
		frames_played_cur = (*i)->fill_buffer(buffer ? temp_buffer : 0, params, num_frames);
		frames_played = std::max(frames_played, frames_played_cur);
		if (buffer) {
			for (unsigned p = 0; p < frames_played_cur * params.channels; ++p) {
				buffer[p] += temp_buffer[p];
			}
		}
	}
	return frames_played;
}

void SumModule::rewind(const SampleParams& params) {
	for (auto i = child_modules.begin(); i != child_modules.end(); ++i)
		(*i)->rewind(params);
	MultiWrapperModule::rewind(params);
}
