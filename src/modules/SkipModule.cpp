#include "SkipModule.h"
#include "SampleParams.h"
#include <algorithm>
#include "more_algorithms.h"
#include <iostream>

SkipModule::SkipModule(double _sec_skip, std::shared_ptr<Module> _child_module):
	WrapperModule(_child_module), sec_skip(_sec_skip) {}

void SkipModule::rewind(const SampleParams& params) {
    WrapperModule::rewind(params);
	if (child_module) {
		unsigned frames_to_skip = sec_skip * params.sample_rate;
		while (frames_to_skip > 0) {
			unsigned s = std::min(frames_to_skip, 4096u);
			child_module->skip_forward(params, s);
			frames_to_skip -= s;
		}
	}
}

RandomSkipModule::RandomSkipModule(double _min_skip, double _max_skip, std::shared_ptr<Module> _child_module):
    SkipModule(0.5 * (_min_skip + _max_skip), _child_module),
    min_skip(_min_skip),
    max_skip(_max_skip)
{}

void RandomSkipModule::rewind(const SampleParams& params) {
    set_sec_skip(rand_ranged(min_skip, max_skip));
    SkipModule::rewind(params);
}


