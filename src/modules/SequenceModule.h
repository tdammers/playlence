#pragma once

#include "MultiWrapperModule.h"

#include <memory>
#include <vector>

class SequenceModule: public MultiWrapperModule {
	public:
		template<typename T> SequenceModule(const T& _child_modules):
			MultiWrapperModule(_child_modules)
			{}
		SequenceModule():
			MultiWrapperModule()
			{}
		virtual Module* clone() const {
			SequenceModule* c = new SequenceModule();
			c->import_children(child_modules);
			return c;
		}
		virtual unsigned fill_buffer(float*, const SampleParams&, unsigned num_frames);
		virtual void rewind(const SampleParams& params);
};
