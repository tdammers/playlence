#include "SineModule.h"

#include "SampleParams.h"

#include <cmath>

SineModule::SineModule(double _freq):
	freq(_freq), phase(0.0)
	{}

unsigned SineModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {
	double delta = freq / (double)params.sample_rate;
	float* bufptr = buffer;
	float value;
	for (unsigned i = 0; i < num_frames; ++i) {
		value = sin(phase * M_PI * 2.0);
		phase += delta;
		for (unsigned c = 0; c < params.channels; ++c) {
			*bufptr++ = value;
		}
	}
	return num_frames;
}
