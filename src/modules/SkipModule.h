#pragma once

#include "WrapperModule.h"
#include <memory>

class SkipModule: public WrapperModule {
	private:
		double sec_skip;
	public:
		SkipModule(double _sec_skip, std::shared_ptr<Module> _child = 0);
		virtual Module* clone() const { return new SkipModule(sec_skip, child_module); }
        inline SkipModule& set_sec_skip(double v) { sec_skip = v; return *this; }
		virtual void rewind(const SampleParams& params);
};

class RandomSkipModule: public SkipModule {
    private:
        double min_skip;
        double max_skip;
    public:
        RandomSkipModule(double _min_skip, double _max_skip, std::shared_ptr<Module> _child = 0);
		virtual void rewind(const SampleParams& params);
};
