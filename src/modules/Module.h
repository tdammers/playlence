#pragma once

struct SampleParams;

class Module {
	public:
		Module() {}
		virtual ~Module() {}
		virtual Module* clone() const = 0;
		virtual unsigned fill_buffer(float*, const SampleParams&, unsigned num_frames) = 0;
		virtual unsigned skip_forward(const SampleParams& params, unsigned num_frames) { return fill_buffer(0, params, num_frames); }
		virtual void rewind(const SampleParams& params) {}
};
