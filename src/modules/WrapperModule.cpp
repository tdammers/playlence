#include "WrapperModule.h"

WrapperModule::WrapperModule(std::shared_ptr<Module> _child_module):
	child_module(_child_module) {}

unsigned WrapperModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {
	if (!child_module) {
		return 0;
	}
    return child_module->fill_buffer(buffer, params, num_frames);
}

void WrapperModule::set_child(std::shared_ptr<Module> _child_module) {
    child_module = _child_module;
}

void WrapperModule::rewind(const SampleParams& params) {
	if (child_module)
		child_module->rewind(params);
}
