#pragma once

#include "SampleModule.h"
#include <vector>
#include <string>

class ShuffleSampleModule: public SampleModule {
	private:
		std::vector<std::string> filenames;
		unsigned current_filename_index;
		bool justOne;
	public:
		ShuffleSampleModule(bool _justOne = false);
		virtual Module* clone() const;
		virtual ShuffleSampleModule& add_filename(const std::string&);
		virtual ShuffleSampleModule& add_glob(const std::string&);
		virtual void rewind(const SampleParams& params);
		virtual unsigned fill_buffer(float*, const SampleParams&, unsigned num_frames);
		virtual void load_current_file();
};
