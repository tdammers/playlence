#include "TimeLimitModule.h"
#include "SampleParams.h"
#include <algorithm>
#include "more_algorithms.h"
#include <iostream>

TimeLimitModule::TimeLimitModule(double _sec_limit, std::shared_ptr<Module> _child_module):
	WrapperModule(_child_module), sec_limit(_sec_limit), sec_position(0.0) {}

unsigned TimeLimitModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {
	if (!child_module)
		return 0;
	unsigned frames_limit = params.sec_to_frames(sec_limit);
	unsigned frames_position = params.sec_to_frames(sec_position);
	if (frames_position >= frames_limit)
		return 0;
	unsigned frames_to_write = std::min(frames_limit - frames_position, num_frames);
	unsigned frames_written = child_module->fill_buffer(buffer, params, frames_to_write);
	frames_position += frames_written;
	sec_position = params.frames_to_sec(frames_position);
	return frames_written;
}

void TimeLimitModule::rewind(const SampleParams& params) {
	sec_position = 0.0;
    WrapperModule::rewind(params);
}

RandomTimeLimitModule::RandomTimeLimitModule(double _min_limit, double _max_limit, std::shared_ptr<Module> _child_module):
    TimeLimitModule(0.5 * (_min_limit + _max_limit), _child_module),
    min_limit(_min_limit),
    max_limit(_max_limit)
{}

void RandomTimeLimitModule::rewind(const SampleParams& params) {
    set_sec_limit(rand_ranged(min_limit, max_limit));
    TimeLimitModule::rewind(params);
}


