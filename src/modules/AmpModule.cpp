#include "AmpModule.h"

#include "SampleParams.h"

unsigned AmpModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {
	unsigned frames_played = WrapperModule::fill_buffer(buffer, params, num_frames);
	if (buffer) {
		for (unsigned i = 0; i < frames_played * params.channels; ++i) {
			buffer[i] *= amp;
		}
	}
	return frames_played;
}
