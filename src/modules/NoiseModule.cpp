#include "NoiseModule.h"
#include "SampleParams.h"
#include <cstdlib>

unsigned NoiseModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {

	if (buffer) {
		for (unsigned i = 0; i < num_frames * params.channels; ++i) {
			buffer[i] = ((float)rand() / (float)RAND_MAX) * 2.0f - 1.0f;
		}
	}
	return num_frames;
}
