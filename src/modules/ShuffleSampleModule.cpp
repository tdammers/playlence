#include "ShuffleSampleModule.h"
#include "glob.h"
#include <algorithm>
#include "SampleParams.h"
#include <iostream>

ShuffleSampleModule::ShuffleSampleModule(bool _justOne): SampleModule(), current_filename_index(0), justOne(_justOne) {}

Module* ShuffleSampleModule::clone() const {
	ShuffleSampleModule* c = new ShuffleSampleModule(justOne);
	for (auto i = filenames.begin(); i != filenames.end(); ++i) {
		c->add_filename(*i);
	}
	return c;
}

ShuffleSampleModule& ShuffleSampleModule::add_filename(const std::string& fn) {
	filenames.push_back(fn);
	return *this;
}

ShuffleSampleModule& ShuffleSampleModule::add_glob(const std::string& pat) {
	auto new_filenames = glob(pat);
	std::copy(
		new_filenames.begin(), new_filenames.end(),
		std::back_inserter(filenames));
	return *this;
}

void ShuffleSampleModule::rewind(const SampleParams& params) {
	current_filename_index = 0;
	std::random_shuffle(filenames.begin(), filenames.end());
	load_current_file();
	SampleModule::rewind(params);
}

void ShuffleSampleModule::load_current_file() {
	if (current_filename_index < filenames.size()) {
		load_sample_file(filenames[current_filename_index]);
	}
	else {
		unload_sample();
	}
}

unsigned ShuffleSampleModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {
	unsigned frames_played = 0;
	while (frames_played < num_frames && current_filename_index < filenames.size()) {
		frames_played += SampleModule::fill_buffer(buffer + (frames_played * params.channels), params, num_frames - frames_played);
		if (frames_played < num_frames) {
			if (justOne) {
				current_filename_index = filenames.size();
			}
			else {
				++current_filename_index;
			}
			load_current_file();
		}
	}
	return frames_played;
}
