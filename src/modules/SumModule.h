#pragma once

#include "MultiWrapperModule.h"

#include <memory>
#include <vector>

class SumModule: public MultiWrapperModule {
	public:
		template<typename T> SumModule(const T& _child_modules):
			MultiWrapperModule(_child_modules)
			{}
		SumModule():
			MultiWrapperModule()
			{}
		virtual unsigned fill_buffer(float*, const SampleParams&, unsigned num_frames);
		virtual Module* clone() const {
			SumModule* c = new SumModule();
			c->import_children(child_modules);
			return c;
		}
		virtual void rewind(const SampleParams& params);
};
