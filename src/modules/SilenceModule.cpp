#include "SilenceModule.h"
#include "SampleParams.h"

unsigned SilenceModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {

	if (buffer) {
		for (unsigned i = 0; i < num_frames * params.channels; ++i) {
			buffer[i] = 0.0;
		}
	}
	return num_frames;
}
