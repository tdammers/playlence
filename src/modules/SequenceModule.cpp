#include "SequenceModule.h"
#include "SampleParams.h"

unsigned SequenceModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {
	unsigned frames_played = 0;
    auto child_module = get_current_child();
	if (!child_module)
		return 0;
	do {
		frames_played += child_module->fill_buffer(
			buffer ? (buffer + params.channels * frames_played) : 0,
			params, num_frames - frames_played);
		if (frames_played < num_frames) {
            child_module = next_child();
            if (at_last_child())
                break;
			child_module->rewind(params);
		}
	} while (frames_played < num_frames);
	return frames_played;
}

void SequenceModule::rewind(const SampleParams& params) {
    first_child();
    MultiWrapperModule::rewind(params);
}
