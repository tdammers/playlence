#include "SampleModule.h"
#include "SampleParams.h"
#include <cstring>
#include <cstdlib>
#include <iostream>

SampleModule::SampleModule():
	sample_filename(), sndfile_error(false), sndfile(0), sfinfo(0)
	{}

SampleModule::SampleModule(const std::string& _sample_filename):
	sample_filename(_sample_filename), sndfile_error(false), sndfile(0), sfinfo(0)
	{}

SampleModule::~SampleModule() {
	unload_sample();
}

unsigned SampleModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {
	load_sample();
	if (!sndfile || !sfinfo) {
		return 0;
	}
	float load_buffer[num_frames * sfinfo->channels];
	unsigned frames_read = sf_readf_float(sndfile, load_buffer, num_frames);
	float* read_ptr = load_buffer;
	float* write_ptr = buffer;
	unsigned i = 0;
	for (i = 0; i < frames_read; ++i) {
		for (unsigned c = 0; c < params.channels; ++c) {
			unsigned rc = c % sfinfo->channels;
			if (write_ptr)
				write_ptr[c] = read_ptr[rc];
		}
		if (write_ptr)
			write_ptr += params.channels;
		read_ptr += sfinfo->channels;
	}
	return frames_read;
}

void SampleModule::load_sample_file(const std::string& _sample_filename) {
	unload_sample();
	sample_filename = _sample_filename;
	sndfile_error = false;
}

void SampleModule::load_sample() {
	if (sndfile_error || sndfile)
		return;
	sfinfo = (SF_INFO*)malloc(sizeof(SF_INFO));
	sndfile = sf_open(sample_filename.c_str(), SFM_READ, sfinfo);
	if (!sndfile) {
		free(sfinfo);
		sfinfo = 0;
		sndfile_error = true;
		std::cerr << "Warning: '" << sample_filename << "' could not be loaded" << std::endl;
	}
}

void SampleModule::unload_sample() {
	sndfile_error = false;
	if (sndfile) {
		sf_close(sndfile);
		sndfile = 0;
	}
	if (sfinfo) {
		free(sfinfo);
		sfinfo = 0;
	}
}

void SampleModule::rewind(const SampleParams& params) {
	// note that if the soundfile hasn't been loaded yet, the sndfile
	// is 0, but that's fine - it'll be loaded when needed.
	if (sndfile) {
		sf_seek(sndfile, 0, SEEK_SET);
	}
}
