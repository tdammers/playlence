#pragma once

#include "Module.h"

#include <memory>

class WrapperModule: public Module {
	protected:
		std::shared_ptr<Module> child_module;
		std::shared_ptr<Module> clone_child() const {
			if (child_module)
				return std::shared_ptr<Module>(child_module->clone());
			else
				return std::shared_ptr<Module>();
		}
    public:
        WrapperModule(std::shared_ptr<Module> _child_module);
        virtual void set_child(std::shared_ptr<Module> _child_module);
		virtual unsigned fill_buffer(float*, const SampleParams&, unsigned num_frames);
        virtual void rewind(const SampleParams& params);
};
