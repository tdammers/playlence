#pragma once

#include <sndfile.h>
#include <string>

#include "Module.h"

class SampleModule: public Module {
	private:
		std::string sample_filename;
		bool sndfile_error;
		SNDFILE* sndfile;
		SF_INFO* sfinfo;
		void load_sample();
	public:
		SampleModule();
		SampleModule(const std::string& _sample_filename);
		virtual ~SampleModule();
		virtual Module* clone() const { return new SampleModule(sample_filename); }
		virtual unsigned fill_buffer(float*, const SampleParams&, unsigned num_frames);
		virtual void load_sample_file(const std::string& sample_filename);
		virtual void unload_sample();
		virtual void rewind(const SampleParams& params);
};
