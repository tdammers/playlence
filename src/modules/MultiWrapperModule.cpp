#include "MultiWrapperModule.h"

MultiWrapperModule::MultiWrapperModule():
    child_modules(),
    current_child_module_i(0)
    {}

void MultiWrapperModule::add_child(std::shared_ptr<Module> m) {
    child_modules.push_back(m);
}

std::shared_ptr<Module> MultiWrapperModule::get_current_child() {
    if (current_child_module_i >= child_modules.size())
        return 0;
    return child_modules[current_child_module_i];
}

void MultiWrapperModule::rewind(const SampleParams& params) {
    auto current_child = get_current_child();
    if (current_child)
        current_child->rewind(params);
}

std::shared_ptr<Module> MultiWrapperModule::next_child() {
    if (current_child_module_i < child_modules.size())
        ++current_child_module_i;
    return get_current_child();
}

std::shared_ptr<Module> MultiWrapperModule::prev_child() {
    if (current_child_module_i > 0)
        --current_child_module_i;
    return get_current_child();
}

std::shared_ptr<Module> MultiWrapperModule::first_child() {
    current_child_module_i = 0;
    return get_current_child();
}
