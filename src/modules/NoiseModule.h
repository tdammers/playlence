#pragma once

#include "Module.h"

class NoiseModule: public Module {
	public:
		virtual unsigned fill_buffer(float* buffer, const SampleParams&, unsigned num_frames);
		virtual Module* clone() const { return new NoiseModule(); }
};
