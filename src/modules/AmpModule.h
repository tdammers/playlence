#pragma once

#include "WrapperModule.h"

class AmpModule: public WrapperModule {
	private:
		double amp;
	public:
		AmpModule(double _amp = 1.0, std::shared_ptr<Module> _child = 0):
			WrapperModule(_child),
			amp(_amp)
			{}
		virtual Module* clone() const { return new AmpModule(amp, clone_child()); }
		virtual unsigned fill_buffer(float*, const SampleParams&, unsigned num_frames);
};
