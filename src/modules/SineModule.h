#pragma once

#include "Module.h"

class SineModule: public Module {
	private:
		double freq;
		double phase;
	public:
		SineModule(double _freq);
		virtual Module* clone() const { return new SineModule(freq); }
		virtual unsigned fill_buffer(float*, const SampleParams&, unsigned num_frames);
		virtual void rewind(const SampleParams& params) { phase = 0.0; }
};
