#include "LoopModule.h"
#include "SampleParams.h"

LoopModule::LoopModule(std::shared_ptr<Module> _child_module, unsigned _loop_limit):
	WrapperModule(_child_module),
	loop_limit(_loop_limit),
	loop_count(0) {}

unsigned LoopModule::fill_buffer(float* buffer, const SampleParams& params, unsigned num_frames) {
	if (!child_module)
		return 0;
	if (loop_limit && loop_count >= loop_limit)
		return 0;

	unsigned frames_played = 0;
	do {
		frames_played += child_module->fill_buffer(buffer + params.channels * frames_played, params, num_frames - frames_played);
		if (frames_played < num_frames) {
			child_module->rewind(params);
			++loop_count;
		}
	} while (frames_played < num_frames && (loop_limit == 0 || loop_count < loop_limit));
	return frames_played;
}

void LoopModule::rewind(const SampleParams& params) {
	WrapperModule::rewind(params);
	loop_count = 0;
}
