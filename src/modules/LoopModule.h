#pragma once

#include "WrapperModule.h"

#include <memory>

class LoopModule: public WrapperModule {
	private:
		unsigned loop_limit;
		unsigned loop_count;
	public:
		LoopModule(std::shared_ptr<Module> _child_module, unsigned _loop_limit = 0);
		virtual Module* clone() const { return new LoopModule(clone_child(), loop_limit); }
		virtual unsigned fill_buffer(float*, const SampleParams&, unsigned num_frames);
		virtual void rewind(const SampleParams& params);
};
