#pragma once

#include "Module.h"

#include <memory>
#include <vector>

class MultiWrapperModule: public Module {
	protected:
		std::vector<std::shared_ptr<Module>> child_modules;
		unsigned current_child_module_i;
	public:
		template<typename T> MultiWrapperModule(const T& _child_modules):
			child_modules(_child_modules),
			current_child_module_i(0)
			{}
		template<typename T> void import_children(const T& _child_modules) { child_modules = std::vector<std::shared_ptr<Module>>(_child_modules); }
		MultiWrapperModule();
		virtual void add_child(std::shared_ptr<Module> m);
        virtual std::shared_ptr<Module> get_current_child();
        virtual std::shared_ptr<Module> next_child();
        virtual std::shared_ptr<Module> prev_child();
        virtual std::shared_ptr<Module> first_child();
        virtual bool at_last_child() const { return current_child_module_i >= child_modules.size(); }
		virtual void rewind(const SampleParams& params);
};
