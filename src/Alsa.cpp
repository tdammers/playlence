#include "Alsa.h"
#include "AlsaException.h"

#include <sstream>

Alsa::Alsa(
	const SampleParams& _sample_params,
	snd_pcm_sframes_t _buffer_size):
	sample_params(_sample_params),
	buffer_size(_buffer_size)
{
	alsa_ecc(
		snd_pcm_open(
			&handle,
			"default",
			SND_PCM_STREAM_PLAYBACK,
			0));
	alsa_ecc(
		snd_pcm_set_params(
			handle,
			SND_PCM_FORMAT_FLOAT,
			SND_PCM_ACCESS_RW_INTERLEAVED,
			sample_params.channels,
			sample_params.sample_rate,
			1,
			500000));
}

Alsa::~Alsa() {
	snd_pcm_close(handle);
}

void Alsa::write(const float* buffer) {
	snd_pcm_sframes_t frames = snd_pcm_writei(handle, buffer, buffer_size);
	if (frames < 0) {
		frames = snd_pcm_recover(handle, frames, 0);
	}
	if (frames < 0) {
		alsa_ecc(frames);
	}
	if (frames > 0 && frames < (long)buffer_size) {
		std::stringstream s;
		s << "Short write (expected " << buffer_size << ", wrote " << frames << ")";
		throw Warning(s.str());
	}
}

void Alsa::wait() {
	snd_pcm_drain(handle);
}
