#pragma once

#include <cmath>

struct SampleParams {
	unsigned channels;
	unsigned sample_rate;
	SampleParams(unsigned _channels, unsigned _sample_rate):
		channels(_channels), sample_rate(_sample_rate)
		{}
	SampleParams():
		channels(1), sample_rate(44100)
		{}
	SampleParams(const SampleParams& rhs):
		channels(rhs.channels), sample_rate(rhs.sample_rate)
		{}
	SampleParams& operator=(const SampleParams& rhs) {
		channels = rhs.channels;
		sample_rate = rhs.sample_rate;
		return *this;
	}
	unsigned sec_to_frames(double sec) const {
		return (unsigned)floor(sec * (double)sample_rate);
	}

	double frames_to_sec(unsigned frames) const {
		return (double)frames / sample_rate;
	}
};
