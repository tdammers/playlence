#pragma once

#include <cstdlib>

template <class I, class J> bool is_prefix_of(I needle_begin, I needle_end, J haystack_begin, J haystack_end) {
    I needle_cur = needle_begin;
    J haystack_cur = haystack_begin;
    while (needle_cur != needle_end) {
        if (*(needle_cur++) != *(haystack_cur++)) {
            return false;
        }
    }
    return true;
}

template <class S, class T> bool is_prefix_of(const S& needle, const T& haystack) {
    return is_prefix_of(needle.begin(), needle.end(), haystack.begin(), haystack.end());
}

template <class CI, class C, class V, typename F> V split_by(CI begin, CI end, F predicate) {
    bool found_any = false;
    C accum;
    V result;
    for (CI i = begin; ; ++i) {
        if (i == end || predicate(*i)) {
            result.push_back(accum);
            accum.clear();
        }
        else {
            accum.push_back(*i);
        }
        if (i == end) {
            break;
        }
    }
    return result;
}

template <class T> T rand_ranged(T min_val, T max_val) {
    return min_val + (T)rand() * (max_val - min_val) / (T)RAND_MAX;
}
