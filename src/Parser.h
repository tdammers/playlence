#pragma once

#include <iostream>
#include <memory>
#include <map>
#include <string>
#include <sstream>

class Module;

typedef std::map<std::string, std::shared_ptr<Module>> Scope;

std::shared_ptr<Module> parseModule(std::istream&);
std::shared_ptr<Module> parseModule(std::istream&, const Scope&);
std::shared_ptr<Module> parseProgram(std::istream& stream, Scope& scope);
std::string parseDefinition(std::istream&, Scope&);
