#include "Player.h"
#include "Exception.h"
#include "AlsaException.h"

using boost::mutex;

class lock {
	private:
		mutex &m;

	public:
		lock(mutex &_m) : m(_m) { _m.lock(); }
		~lock() { m.unlock(); }
};


void Player::set_module(shared_ptr<Module> module) {
	lock l(mtx);
	SampleParams params(options.channels, options.sample_rate);

	current_module = module;
	if (current_module)
		current_module->rewind(params);
}

void clear_buffer(float* buffer, const Options& options, unsigned offset = 0) {
	for (auto i = offset * options.channels; i < options.buffer_size * options.channels; ++i) {
		buffer[i] = 0.0;
	}
}

void Player::run() {
	unsigned frames_played;
	float buffer[options.buffer_size * options.channels];
	SampleParams params(options.channels, options.sample_rate);

	Alsa alsa(params, options.buffer_size);
	{
		lock l(mtx);
		if (current_module)
			current_module->rewind(params);
	}
	do {
		try {
			frames_played = 0;
			if (rolling) {
				{
					lock l(mtx);
					if (current_module) {
						frames_played = current_module->fill_buffer(buffer, alsa.get_sample_params(), options.buffer_size);
					}
				}
				if (frames_played < options.buffer_size) {
					// module has reached its end point
					if (!options.interactive) {
						// in non-interactive mode, exit when done.
						exit_flag = true;
					}
					// clear out the rest of the buffer.
					clear_buffer(buffer, options, frames_played);
				}
			}
			else {
				// clear the buffer to avoid looping garbage.
				clear_buffer(buffer, options);
			}
			alsa.write(buffer);
		}
		catch (Warning& e) {
			cerr << e.get_msg() << endl;
		}
	} while (!exit_flag);
	alsa.wait();
}
