#pragma once

#include <string>

class Exception {
	private:
		std::string msg;
	public:
		Exception(const std::string& _msg = "Exception"): msg(_msg) {}
		const std::string& get_msg() const { return msg; }
};

class Warning: public Exception {
	public:
		Warning(const std::string& _msg = "Something worth noticing happened"): Exception("Warning: " + _msg) {}
};
