#include "AlsaException.h"
#include <alsa/asoundlib.h>

void alsa_ecc(int err) {
	if (err) {
		throw AlsaException(err);
	}
}
