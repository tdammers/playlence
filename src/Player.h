#pragma once

#include "Alsa.h"
#include "Module.h"
#include "Options.h"
#include <boost/thread/mutex.hpp>
#include <memory>

using namespace std;

class Player {
	private:
		boost::mutex mtx;
		shared_ptr<Module> current_module;
		const Options& options;
		bool rolling;
		bool exit_flag;
	
	public:
		Player(const Options& _options):
			mtx(), current_module(), options(_options),
			rolling(false), exit_flag(false)
		{ }

		void set_module(shared_ptr<Module> module);

		void start() { rolling = true; }
		void stop() { rolling = false; }
		void terminate() { exit_flag = true; }

		void run();
};


