#include "Parser.h"
#include "ParserErrors.h"
#include <cctype>
#include "AllModules.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <sstream>

using std::cerr;
using std::endl;

class Parser {
	private:
		struct seekpos {
			std::streampos sp;
			unsigned src_line;
			unsigned src_col;
			seekpos(std::streampos _sp, unsigned _src_line, unsigned _src_col):
				sp(_sp), src_line(_src_line), src_col(_src_col) {}
			seekpos(const seekpos& rhs):
				sp(rhs.sp), src_line(rhs.src_line), src_col(rhs.src_col) {}
			seekpos& operator=(const seekpos& rhs) {
				sp = rhs.sp;
				src_line = rhs.src_line;
				src_col = rhs.src_col;
				return *this;
			}

			std::string to_string() const {
				std::stringstream s;
				s << src_line << ":" << src_col << " (" << sp << ")";
				return s.str();
			}
		};

		std::istream& stream;
		unsigned src_line;
		unsigned src_col;

		seekpos tell() const {
			return seekpos(stream.tellg(), src_line, src_col);
		}

		void seek(const seekpos& s) {
			stream.clear();
			stream.seekg(s.sp);
			src_line = s.src_line;
			src_col = s.src_col;
		}

		int peekc() {
			return stream.peek();
		}

		int getc() {
			if (!stream.good()) {
				return stream.peek();
			}
			int c = stream.get();
			switch (c) {
				case '\n':
					src_line++;
					src_col = 1;
					break;
				default:
					src_col++;
					break;
			}
			return c;
		}


	public:
		Parser(std::istream& _stream): stream(_stream), src_line(1), src_col(1) {}

		std::string definitionP(Scope& scope) {
			std::string ident;

			tryVP([&]() {
				spaceP();
				charP('@');
				ident = identP();
				spaceP();
				charP('=');
				spaceP();
				std::shared_ptr<Module> module = moduleP(scope);
				scope[ident] = module;
			});
			return ident;
		}

		std::shared_ptr<Module> varRefP(const Scope& scope) {
			charP('@');
			auto ident = identP();
			spaceP();
			auto var_ref = scope.find(ident);
			if (var_ref != scope.end()) {
				return std::shared_ptr<Module>(var_ref->second->clone());
			}
			throw NoParse("Variable '@" + ident + "' is not defined");
		}

		void expected(const std::string& what) {
			if (stream.good())
				expected(what, peekc());
			else
				expected(what, "end of input");
		}

		void expected(const std::string& what, char actual) {
			std::string actualStr;
			actualStr += actual;
			expected(what, "'" + actualStr + "'");
		}

		void expected(const std::string& what, const std::string& actual) {
			throw UnmetExpectation(src_line, src_col, "", what, actual);
		}

		void eofP() {
			if (stream.good()) {
				expected("end of input");
			}
		}

		std::shared_ptr<Module> tryJustModuleP(const Scope& scope) {
			std::shared_ptr<Module> m;
			tryVP([&](){
				m = moduleP(scope);
				spaceP();
				eofP();
			});
			return m;
		}

		std::shared_ptr<Module> moduleP(const Scope& scope) {
			spaceP();
			if (!stream.good()) {
				throw IncompleteParse();
			}
			switch (peekc()) {
				case '\'':
				case '"':
					return smpShorthandP(scope);
				case '[':
					return seqShorthandP(scope);
				case '@':
					return varRefP(scope);
				case '{':
					return scopeBlockP(scope);
				case '(': {
						getc();
						spaceP();
						auto m = moduleP(scope);
						spaceP();
						charP(')');
						spaceP();
						return m;
					}
					break;
			}
			auto ident = identP();
			if (ident == "loop") {
				return loopArgsP(scope);
			}
			if (ident == "amp") {
				return ampArgsP(scope);
			}
			if (ident == "seq") {
				return seqArgsP(scope);
			}
			if (ident == "sum" || ident == "parallel") {
				return sumArgsP(scope);
			}
			if (ident == "for" || ident == "time") {
                return timeLimitArgsP(scope);
			}
			if (ident == "skip") {
				return skipArgsP(scope);
			}
			if (ident == "smp") {
				return smpArgsP(scope);
			}
			if (ident == "shuffle") {
				return shuffleArgsP(scope, false);
			}
			if (ident == "any") {
				return shuffleArgsP(scope, true);
			}
			if (ident == "silence") {
				return std::shared_ptr<Module>(new SilenceModule());
			}
			if (ident == "noise") {
				return std::shared_ptr<Module>(new NoiseModule());
			}
			throw NoParse("Invalid keyword '" + ident + "'");
		}

		std::shared_ptr<Module> scopeBlockP(const Scope& scope) {
			// Establish a new context
			Scope new_scope(scope);

			charP('{');
			spaceP();
			// some optional definitions
			definitionsP(new_scope);
			spaceP();
			auto m = moduleP(new_scope);
			spaceP();
			charP('}');
			spaceP();
			return m;
		}

		void definitionsP(Scope& scope) {
			while (1) {
				try {
					BacktraceWrapper w(*this);

					definitionP(scope);
					// optionally separated by semicolons
					spaceP();
					if (peekc() == ';')
						getc();
					spaceP();
					w.confirm();
				}
				catch (const ParserError& e) {
					break;
				}
			}
		}

		void importsP(Scope& scope) {
			while (importP(scope))
				;
		}

		bool importP(Scope& scope) {
			BacktraceWrapper w(*this);

			std::string ident;
			try {
				ident = identP();
			}
			catch (const NoParse& e) {
				return false;
			}
			if (ident != "import") {
				return false;
			}
			spaceP();
			std::string filename = stringLiteralP();
			spaceP();
			if (peekc() == ';') getc();
			spaceP();
			w.confirm();
			std::fstream fin(filename);
			cerr << "Load " << filename << "..." << endl;
			if (fin.good()) {
				parseProgram(fin, scope);
				cerr << filename << ": OK" << endl;
			}
			else {
				cerr << filename << ": FAILED!" << endl;
			}
			return true;
		}

		std::shared_ptr<Module> smpShorthandP(const Scope& scope) {
			return tryDefP([&](){
				auto filename = stringLiteralP();
				return std::shared_ptr<Module>(new SampleModule(filename));
			}, std::shared_ptr<Module>());
		}

		std::shared_ptr<Module> seqShorthandP(const Scope& scope) {
			return std::shared_ptr<Module>(new SequenceModule(moduleListP(scope)));
		}

		std::shared_ptr<Module> timeLimitArgsP(const Scope& scope) {
            double min_limit, max_limit;
			spaceP();
			charP('(');
			spaceP();
			min_limit = floatLiteralP();
			spaceP();
			charP(',');
			spaceP();
			max_limit =
				tryDefP([&] () {
					double val = floatLiteralP();
					charP(',');
					spaceP();
					return val;
				}, min_limit);
			std::shared_ptr<Module> inner(moduleP(scope));
			spaceP();
			charP(')');
            if (max_limit == min_limit) {
                return std::shared_ptr<Module>(new TimeLimitModule(min_limit, inner));
            }
            else {
                return std::shared_ptr<Module>(new RandomTimeLimitModule(min_limit, max_limit, inner));
            }
		}

		std::shared_ptr<Module> skipArgsP(const Scope& scope) {
            double min_limit, max_limit;
			spaceP();
			charP('(');
			spaceP();
			min_limit = floatLiteralP();
			spaceP();
			charP(',');
			spaceP();
			max_limit =
				tryDefP([&] () {
					double val = floatLiteralP();
					charP(',');
					spaceP();
					return val;
				}, min_limit);
			std::shared_ptr<Module> inner(moduleP(scope));
			spaceP();
			charP(')');
            if (max_limit == min_limit) {
                return std::shared_ptr<Module>(new SkipModule(min_limit, inner));
            }
            else {
                return std::shared_ptr<Module>(new RandomSkipModule(min_limit, max_limit, inner));
            }
		}

		std::shared_ptr<Module> smpArgsP(const Scope& scope) {
			spaceP();
			charP('(');

			std::shared_ptr<SampleModule> result(new SampleModule());
			spaceP();
			auto filename = stringLiteralP();
			spaceP();
			result->load_sample_file(filename);
			spaceP();
			charP(')');

			return result;
		}

		std::shared_ptr<Module> shuffleArgsP(const Scope& scope, bool justOne) {
			spaceP();
			charP('(');

			std::shared_ptr<ShuffleSampleModule> result(new ShuffleSampleModule(justOne));
			try {
				tryVP([&] () {
					spaceP();
					auto glob = stringLiteralP();
					spaceP();
					result->add_glob(glob);
				});
			}
			catch (NoParse& e) {
				result = 0;
			}

			spaceP();
			charP(')');

			return result;
		}

		std::shared_ptr<Module> ampArgsP(const Scope& scope) {
			spaceP();
			charP('(');

			double amp = floatLiteralP();
			spaceP(); charP(','); spaceP();

			std::shared_ptr<Module> inner =
				tryDefP([&](){ return moduleP(scope); }, std::shared_ptr<Module>());

			spaceP(); charP(')');

			return std::shared_ptr<AmpModule>(new AmpModule(amp, inner));
		}

		std::shared_ptr<Module> loopArgsP(const Scope& scope) {
			spaceP();
			charP('(');

			unsigned loop_limit = tryDefP([&](){
				auto val = uintLiteralP();
				spaceP(); charP(','); spaceP();
				return val;
				}, 0);

			std::shared_ptr<Module> inner = moduleP(scope);

			spaceP(); charP(')');

			return std::shared_ptr<LoopModule>(new LoopModule(inner, loop_limit));
		}

		std::shared_ptr<Module> seqArgsP(const Scope& scope) {
			spaceP();
			charP('(');
			spaceP();
			auto children = moduleListP(scope);
			spaceP();
			charP(')');
			return std::shared_ptr<SequenceModule>(new SequenceModule(children));
		}

		std::shared_ptr<Module> sumArgsP(const Scope& scope) {
			spaceP();
			charP('(');
			spaceP();
			auto children = moduleListP(scope);
			spaceP();
			charP(')');
			return std::shared_ptr<SumModule>(new SumModule(children));
		}

		// A list of modules, separated by commas and surrounded by square
		// brackets.
		std::vector<std::shared_ptr<Module>> moduleListP(const Scope& scope) {
			std::vector<std::shared_ptr<Module>> result;

			charP('[');
			spaceP();
			while (stream.good()) {
				if (peekc() == ']')
					break;
				result.push_back(moduleP(scope));
				spaceP();
				if (peekc() == ']')
					break;
				charP(',');
				spaceP();
			}
			charP(']');
			return result;
		}

		// skip whitespace
		void spaceP() {
			while (stream.good() && isspace(peekc())) {
				getc();
			}
		}

		// match a single character
		char charP(char c) {
			if (!stream.good())
				throw IncompleteParse();
			if (peekc() != c)
				expected("'" + std::string(1, c) + "'");
			return getc();
		}

		template<class ReturnT, class WrappedT, class LeftT, class RightT>
		ReturnT betweenP(LeftT leftP, RightT rightP, WrappedT wrappedP) {
			leftP();
			ReturnT retval = wrappedP();
			rightP();
			return retval;
		}

		class BacktraceWrapper {
			private:
				Parser& p;
				seekpos sp;
				bool confirmed;
			public:
				BacktraceWrapper(Parser& _p): p(_p), sp(_p.tell()), confirmed(false) {}
				~BacktraceWrapper() {
					if (!confirmed) p.seek(sp);
				}
				void confirm() { confirmed = true; }
		};

		template<class WrappedT>
		void tryVP(WrappedT wrappedP) {
			BacktraceWrapper w(*this);
			wrappedP();
			w.confirm();
		}

		template<class ReturnT, class WrappedT>
		ReturnT defP(WrappedT wrappedP, ReturnT defVal) {
			try {
				return wrappedP();
			}
			catch (ParserError& e) {
				return defVal;
			}
		}

		template<class ReturnT, class WrappedT>
		ReturnT tryDefP(WrappedT wrappedP, ReturnT defVal) {
			try {
				BacktraceWrapper w(*this);
				ReturnT r = wrappedP();
				w.confirm();
				return r;
			}
			catch (ParserError& e) {
				return defVal;
			}
		}

		template<class ReturnT, class WrappedT>
		ReturnT parensP(WrappedT wrappedP) {
			charP('(');
			spaceP();
			ReturnT retval = wrappedP();
			spaceP();
			charP(')');
			return retval;
		}

		// parse an identifier
		std::string identP() {
			std::string result("");
			if (!isalpha(peekc()) && peekc() != '_') {
				expected("letter or _");
			}
			result += getc();
			while (stream.good() && (isalnum(peekc()) || peekc() == '_')) {
				result += getc();
			}
			return result;
		}

		char resolve_escape(char esc2) {
			switch (esc2) {
				case 'n': return '\n';
				case 't': return '\t';
				case 'v': return '\v';
				case 'b': return '\b';
				case '0': return '\0';
				default: return esc2;
			}
		}

		// A string literal, enclosed in single or double quotes.
		std::string stringLiteralP() {
			char sep = peekc();
			if (sep != '"' && sep != '\'')
				expected("string literal");
			charP(sep);
			std::string result;
			while (stream.good() && peekc() != sep) {
				char c = getc();
				if (c == '\\') {
					if (!stream.good()) {
						throw IncompleteParse();
					}
					c = getc();
					result += resolve_escape(c);
				}
				else {
					result += c;
				}
			}
			charP(sep);
			return result;
		}

		unsigned uintLiteralP() {
			std::string strval;
			while (stream.good() && peekc() >= '0' && peekc() <= '9') {
				strval += getc();
			}
			if (strval.empty()) {
				expected("unsigned-integer literal");
			}
			return (unsigned)atol(strval.c_str());
		}

		int intLiteralP() {
			bool negative = false;
			if (peekc() == '-') {
				negative = true;
				getc();
			}
			else if (peekc() == '+') {
				getc();
			}
			int i = uintLiteralP();
			if (negative)
				return -i;
			else
				return i;
		}

		double floatLiteralP() {
			std::string strval;
			while (stream.good() && peekc() >= '0' && peekc() <= '9') {
				strval += getc();
			}
			if (peekc() == '.') {
				strval += getc();
				while (stream.good() && peekc() >= '0' && peekc() <= '9') {
					strval += getc();
				}
			}
			if (strval.empty()) {
				expected("floating-point literal");
			}
			return atof(strval.c_str());
		}

	std::shared_ptr<Module> programP(Scope& scope) {
		importsP(scope);
		definitionsP(scope);
		if (stream.good()) {
			return moduleP(scope);
		}
		else {
			return std::shared_ptr<Module>();
		}
	}
};

std::shared_ptr<Module> parseModule(std::istream& stream) {
	Parser parser(stream);
	Scope scope;
	return parser.moduleP(scope);
}

std::shared_ptr<Module> parseModule(std::istream& stream, const Scope& global_scope) {
	Parser parser(stream);
	Scope scope(global_scope);
	return parser.tryJustModuleP(scope);
}

std::shared_ptr<Module> parseProgram(std::istream& stream, Scope& scope) {
	Parser parser(stream);
	return parser.programP(scope);
}

std::string parseDefinition(std::istream& stream, Scope& scope) {
	Parser parser(stream);
	return parser.definitionP(scope);
}
