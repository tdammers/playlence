#pragma once

#include <alsa/asoundlib.h>
#include "SampleParams.h"

class Alsa {
	private:
		SampleParams sample_params;
		snd_pcm_t* handle;
		snd_pcm_sframes_t buffer_size;
	public:
		Alsa(const SampleParams& sample_params, snd_pcm_sframes_t buffer_size);
		~Alsa();
		void write(const float*);
		void wait();

		inline const SampleParams& get_sample_params() const { return sample_params; }
		inline unsigned get_buffer_size() const { return buffer_size; }
};
