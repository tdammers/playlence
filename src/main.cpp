#include "src-gen/license.h"

#include "Alsa.h"
#include "AlsaException.h"
#include "Exception.h"
#include "Module.h"
#include "Options.h"
#include "Parser.h"
#include "ParserErrors.h"
#include "Player.h"

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <boost/thread/thread.hpp>

using boost::thread;

shared_ptr<Module> loadModuleFromFile(const string& filename, Scope& variables) {
	ifstream fin(filename);
	return parseProgram(fin, variables);
}

shared_ptr<Module> loadModuleFromStdin(Scope& variables) {
	stringstream ssin;
	while (cin.good())
		ssin.put(cin.get());
	stringstream ssout(ssin.str());
	return parseProgram(ssout, variables);
}

bool stepInteractively(Player& player, Scope& variables) {
	string str;

	cerr << "playlence> " << flush;

	while (cin.good()) {
		string ln;
		getline(cin, ln);
		if (ln == "\\c") {
			return true; // cancel command
		}
		if (cin.eof()) {
			cerr << endl;
			cerr << "Bye!" << endl;
			player.terminate();
			return false;
		}
		// append current line to input buffer
		str += ln;

		if (!str.empty() && str[0] == '\\') {
			// catch some special commands
			if (str == "\\start") {
				player.start();
				cerr << "Player started" << endl;
				return true;
			}
			if (str == "\\stop") {
				player.stop();
				cerr << "Player stopped" << endl;
				return true;
			}
			if (str == "\\quit" || str == "\\q" || str == "\\bye") {
				cerr << "Bye!" << endl;
				player.terminate();
				return false;
			}
			if (str == "\\ls") {
				cerr << "-- Currently defined variables: --" << endl;
				for (auto i = variables.begin(); i != variables.end(); ++i) {
					cerr << "    @" << i->first << endl;
				}
				return true;
			}
			if (str == "\\reset") {
				player.set_module(0);
				variables.clear();
				return true;
			}
			cerr << "Unrecognized command: " << str << endl;
			return true;
		}
		try {
			stringstream s(str);
			auto m = parseProgram(s, variables);
			if (m) {
				player.set_module(m);
				cerr << "Now playing: " << str << endl;
			}
			return true;
		}
		catch (const IncompleteParse& e) {
			cerr << str << endl << "...> " << flush;
		}
		catch (const ParserError& e) {
			cerr << e.report() << endl;
			return true;
		}
		catch (std::string& e) {
			cerr << "Error: " << e << endl;
			return true;
		}
	}
	cerr << "Bye!" << endl;
	return false;
}

struct PlayerRunner {
	Player& player;

	PlayerRunner(Player& _player):
		player(_player) {}

	void operator() () {
		player.run();
	}
};

void play(shared_ptr<Module> module, const Options& options) {
	Player player(options);
	player.set_module(module);
	player.start();
	player.run();
}

void runInteractively(Options& options) {
	Player player(options);
	PlayerRunner runner(player);
	thread player_thread(runner);
	Scope variables;
	shared_ptr<Module> main_module;

	if (!options.fromStdin)
		main_module = loadModuleFromFile(options.inputFilename, variables);
	if (main_module)
		player.set_module(main_module);
	player.start();
	do {} while (stepInteractively(player, variables));
	player_thread.join();
}

int main(int argc, const char* argv[]) {
	Options options;

	options.parse(argv + 1, argv + argc);

	try {
		shared_ptr<Module> main_module;
		if (options.interactive) {
			runInteractively(options);
		}
		else {
			Scope global_scope;
			if (options.fromStdin) {
				main_module = loadModuleFromStdin(global_scope);
			}
			else {
				main_module = loadModuleFromFile(options.inputFilename, global_scope);
			}
			cerr << "OK" << endl << "Now playing..." << endl;

			play(main_module, options);

			cerr << "Done!" << endl;
		}

		return 0;
	}
	catch (ParserError& e) {
		cerr << e.report() << endl;
	}
	catch (Exception& e) {
		cerr << e.get_msg() << endl;
		return -1;
	}
}
