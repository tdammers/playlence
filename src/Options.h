#pragma once

#include <string>
#include <vector>
#include <iostream>

#include "more_algorithms.h"

struct Options {
    unsigned sample_rate;
    unsigned buffer_size;
    unsigned channels;

    bool interactive;
    bool fromStdin;
    std::string inputFilename;

    Options():
        sample_rate(44100),
        buffer_size(1024),
        channels(2),
        interactive(false),
        fromStdin(true),
        inputFilename()
        {}

    template<class I> Options& parse(I first, I end) {
        I cur = first;
        while (cur != end) {
            std::string token = *(cur++);
            if (is_prefix_of(std::string("--"), token)) {
                // long arg
                auto arg_name = token.substr(2);
            }
            else if (is_prefix_of(std::string("-"), token)) {
                // short args
                for (auto i = token.begin() + 1; i != token.end(); ++i) {
                    switch (*i) {
                        case 'i':
                            interactive = true;
                            break;
                        default:
                            std::cerr << "Warning: unknown option '-" << *i << "'" << std::endl;
                    }
                }
            }
            else {
                // free arg
                inputFilename = token;
                fromStdin = false;
            }
        }
        return *this;
    }
};
