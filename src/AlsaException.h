#pragma once

#include "Exception.h"

#include <alsa/asoundlib.h>

class AlsaException: public Exception {
	public:
		AlsaException(int err):
			Exception("ALSA Error: " + (std::string)snd_strerror(err)) {}
};

/**
 * Error-checked ALSA call.
 * 
 * @param int err An ALSA error code, typically a return value from an ALSA
 * function. If nonzero, this function throws an AlsaException.
 */
void alsa_ecc(int err);
