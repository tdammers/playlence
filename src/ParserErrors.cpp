#include "ParserErrors.h"

#include <sstream>

std::string ParserError::report() const {
	std::stringstream ss;
	if (!src_file.empty())
		ss << src_file;
	else
		ss << "<<unknown>>";
	if (src_line) ss << ":" << src_line;
	if (src_col) ss << ":" << src_col;
	ss << ": " << get_type_name() << ": " << get_msg();
	return ss.str();
}

UnmetExpectation::UnmetExpectation(const std::string& _expected, const std::string& _actual):
	UnmetExpectation(0, 0, "", _expected, _actual)
{}

UnmetExpectation::UnmetExpectation(
		int _src_line,
		int _src_col,
		const std::string& _src_file,
		const std::string& _expected,
		const std::string& _actual):
	NoParse(_src_line, _src_col, _src_file,
	"Expected " + _expected + (_actual.empty() ? "" : (", but found " + _actual))),
	expected(_expected),
	actual(_actual)
{}
