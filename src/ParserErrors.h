#pragma once

#include <string>

class ParserError {
	private:
		int src_line;
		int src_col;
		std::string src_file;
		std::string msg;
	public:
		ParserError(const std::string& _msg = "Error"):
			ParserError(0, 0, "", _msg) {}
		ParserError(int _src_line, int _src_col, const std::string& _src_file = "", const std::string& _msg = "Error"):
			src_line(_src_line),
			src_col(_src_col),
			src_file(_src_file),
			msg(_msg) {}
		virtual ~ParserError() { }
		virtual std::string get_msg() const { return msg; }
		virtual std::string report() const;
		virtual std::string get_type_name() const { return "ParserError"; }
};

class NoParse: public ParserError {
	public:
		NoParse(const std::string& _msg = "No Parse"):
			ParserError(_msg) {}
		NoParse(int _src_line, int _src_col, const std::string& _src_file = "", const std::string& _msg = "No Parse"):
			ParserError(_src_line, _src_col, _src_file, _msg) {}
		virtual std::string get_type_name() const { return "NoParse"; }
};

class IncompleteParse: public ParserError {
	public:
		IncompleteParse(const std::string& _msg = "Incomplete Parse"):
			ParserError(_msg) {}
		IncompleteParse(int _src_line, int _src_col, const std::string& _src_file = "", const std::string& _msg = "Incomplete Parse"):
			ParserError(_src_line, _src_col, _src_file, _msg) {}
		virtual std::string get_type_name() const { return "IncompleteParse"; }
};

class UnmetExpectation: public NoParse {
	private:
		std::string expected;
		std::string actual;
	public:
		UnmetExpectation(const std::string& _expected, const std::string& _actual = "");
		UnmetExpectation(
				int _src_line,
				int _src_col,
				const std::string& _src_file,
				const std::string& _expected,
				const std::string& _actual = "");
		std::string get_expected() const { return expected; }
		std::string get_actual() const { return actual; }
		virtual std::string get_type_name() const { return "UnmetExpectation"; }
};

