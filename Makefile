CC=gcc
CXX=g++
LD=g++
INSTALL=install
CXXFLAGS=-c -Wall -Werror
CXXFLAGS+=-O2
# CXXFLAGS+=-g -ggdb
INCLUDEFLAGS=-I . -I src -I src-gen -I src/modules
CXXFLAGS+=$(INCLUDEFLAGS)
CXXFLAGS+=-std=c++0x
LDFLAGS=-O2
SOURCES=$(wildcard src/*.cpp) $(wildcard src/modules/*.cpp)
OBJECTS=$(patsubst src/%.cpp,obj/%.o,$(SOURCES))
LIBS=asound sndfile boost_system boost_thread
LOADLIBES=$(patsubst %,-l%, $(LIBS))
EXECUTABLE=playlence
GENERATED_SOURCE_DEPENDENCIES=$(wildcard src/modules/*.h) $(wildcard generators/*) LICENSE
PREFIX=/usr/local

all: $(SOURCES) $(EXECUTABLE) depend

install: $(EXECUTABLE)
	$(INSTALL) $(EXECUTABLE) $(PREFIX)/bin/$(EXECUTABLE)

uninstall:
	rm -f /usr/local/bin/$(EXECUTABLE)
.PHONY: uninstall

clean:
	rm -f $(EXECUTABLE) $(OBJECTS)
.PHONY: clean

veryclean: clean
	rm -f depend
	rm -f src-gen/*
.PHONY: veryclean

obj/%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) $< -o $@

obj/modules/%.o: src/modules/%.cpp
	$(CXX) $(CXXFLAGS) $< -o $@

depend: $(wildcard src/*.h) $(wildcard src/modules/*.h) $(GENERATED_SOURCE_DEPENDENCIES) $(SOURCES) Makefile
	$(CXX) $(wildcard src/*.cpp) $(INCLUDEFLAGS) -MM -MG \
	| sed -e '/^\w/s/^/obj\//' \
	> depend
	$(CXX) $(wildcard src/modules/*.cpp) $(INCLUDEFLAGS) -MM -MG \
	| sed -e '/^\w/s/^/obj\/modules\//' \
	>> depend

$(EXECUTABLE): $(OBJECTS)
	$(LD) $(LDFLAGS) $(OBJECTS) $(LOADLIBES) -o $@

license.h: src-gen/license.h
src-gen/license.h: LICENSE
	sed LICENSE -e 's/^\(.*\)$$/"\1\\n" \\/' -e '1i#define LICENCE_TEXT \\' -e '$$a""' > src-gen/license.h

AllModules.h: src-gen/AllModules.h
src-gen/AllModules.h: generators/mkAllModules.h.sh
	./generators/mkAllModules.h.sh src-gen/AllModules.h

include depend
