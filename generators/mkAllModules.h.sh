#!/usr/bin/env bash
cd $(dirname $0)
cd ..

MODULE_FILES=src/modules/*.h

(
echo '#pragma once'
echo ''
for FILE in $MODULE_FILES
	do
		BASENAME=$(basename "$FILE")
		echo "#include \"$BASENAME\""
	done
) > "$1"
